= Redmine検索の未来像

Redmineの検索はどうなるといいだろう！

== ライセンス

=== スライド

CC BY-SA 4.0

原著作者：須藤功平

==== Groongaのロゴ

CC BY 3.0

原著作者：Groongaプロジェクト

ページヘッダーで使っています。

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide-kou-redmine-tokyo-17

=== 表示

  rabbit rabbit-slide-kou-redmine-tokyo-17.gem

